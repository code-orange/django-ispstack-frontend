from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path
from django.views.generic.base import RedirectView

from django_ispstack_admin.django_ispstack_admin import urls as admin_pages
from django_ispstack_dashboard.django_ispstack_dashboard import urls as dashboard_pages

urlpatterns = [
    path("", RedirectView.as_view(url="/dashboard", permanent=True)),
    path("", include(admin_pages)),
    path("", include(dashboard_pages)),
    path("account/", include("django.contrib.auth.urls")),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
